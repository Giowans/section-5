import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';
import Components from './src/screens/componentScreen';
import ListitaFlat from './src/screens/ListScreen';
import ImageScreen from './src/screens/ImageScreen';

const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    Componentes: Components,
    ListScreen: ListitaFlat,
    ImgScreen: ImageScreen
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'Seccion 5 '
    }
  }
);

export default createAppContainer(navigator);
