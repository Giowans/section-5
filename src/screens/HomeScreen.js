import React from 'react';
import { Text, StyleSheet, View, Button, TouchableOpacity} from 'react-native';

const HomeScreen = ({navigation}) => {
  return(
    <View>
      <Text style={styles.text}>Botones con Navegacion :D</Text>
      <Button
        title = "Vamos a los componentes :D" 
        onPress = {()=> navigation.navigate('Componentes')}
      />
      <Button
        title = "Vamos a la Listita" 
        onPress = {()=> navigation.navigate('ListScreen')}
      />
      <Button
        title = "Vamos a la ImageScreen" 
        onPress = {()=> navigation.navigate('ImgScreen')}
      />          
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;
