# Sección #5: Aprendiendo a generar mis propios componentes :D

¡Es realmente sencillo ver como podemos estructurar pantallas con componentes! realmente el manejo de datos con Javascript mas los componentes primitivos que nos brinda React Native podemos lograr cosas bastante decentes, tal y como aprendi en esta sección.
Basicamente el objetivo fue crear un componente abstracto que en base a algunos parametros dados se mostrara diferente. El componente es un contenedor de una imagen mas algunas descripciones: y estos detalles son recibidos a travéz de la instancia del componente con JSX:

```jsx
import React from 'react';
import {Text, StyleSheet, Image, View} from 'react-native';

const ImageDetails = (props) => {
    return (
        <View>
            <Image source= {props.imgSource} style = {styles.imgStyle}></Image>
            <View>
                <Text style= {styles.textStyle}>{props.titulo}{'\n'}
                    <Text style= {{backgroundColor: 'khaki'}}>Score: {props.score}</Text>
                </Text>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    textStyle:{
        textAlign: "center",
        fontSize: 20,
        backgroundColor: "aquamarine"
    },
    imgStyle:{
        alignContent: "flex-start",
        paddingTop: 10
    }
});

export default ImageDetails;
```
Este componente se encuentra en `ImageDetails.js` y nos es posible utilizarlo las veces que queramos simplemente importandolo desde este archivo. Cabe destacar que el componente tiene una estructura, pero puede lucir diferente en base a sus parametros. Por ejemplo:

```jsx
const ImageScreen = () => {
    return (
        <View>
            <ImageDetails titulo = "Bosque" imgSource = {require('../img/forest.jpg')} score = "10"></ImageDetails>
            <ImageDetails titulo = "Playa" imgSource = {require('../img/beach.jpg')} score = "10"></ImageDetails>
            <ImageDetails titulo = "Montaña" imgSource = {require('../img/mountain.jpg')}score = "9.5"></ImageDetails>
        </View>
    );
};
```
En el `ImageScreen.js` creamos nuestro componente que es básicamente es como se ve nuestra pantalla: dentro de el hay tres instancias del componente que creamos anteriormente.
Cada uno tiene una imagen y un texto que describe el nombre de la imagen y su puntuación (un suponer xd), sin embargo cada una de estas cosas es diferente ya que se mandan como parametros.